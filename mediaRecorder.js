export default class AudioRecorderWithMediaRecorder {
  constructor(ms) {
    this.mediaStream = ms;
    this.mediaRecorder = new MediaRecorder(this.mediaStream);
    this.audioContext = new AudioContext();
    this.chunks = [];
    this.audioBlob = null;
    this.mediaRecorder.ondataavailable = (evt) => {
      // push each chunk (blobs) in an array
      this.chunks.push(evt.data);
    };
  }

  record() {
    this.mediaRecorder.start();
  }

  stop() {
    return new Promise((res, rej) => {
      this.mediaRecorder.stop();
      this.mediaRecorder.onstop = (evt) => {
        // Make blob out of our blobs, and open it.
        var blob = new Blob(this.chunks, { 'type': 'audio/ogg; codecs=opus' });
        document.querySelector("audio").src = URL.createObjectURL(blob);
        this.audioBlob = blob;
        res(blob);
      };
    });
  }

  checkState() {
    return this.mediaRecorder.state;
  }

  getAudio() {
    return this.audioBlob;
  }

}