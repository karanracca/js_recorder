export default class AudioRecorderWithContext {
  constructor(stream) {
    this.state = 'inactive';
    let AudioContext = window.AudioContext || window.webkitAudioContext;
    this.audioContext = new AudioContext;
    this.source = this.audioContext.createMediaStreamSource(stream);
    this.sampleRate = this.audioContext.sampleRate;
    this.leftChannel = [];
    this.rightChannel = [];
    this.recordingLength = 0;
    this.audioNode = null;
    this.sampleRate = null;
  }

  record() {
    try {
      this.audioNode = this.audioContext.createScriptProcessor(4096, 2, 2);
      this.audioNode.connect(this.audioContext.destination);
      this.source.connect(this.audioNode);
      this.audioNode.onaudioprocess = (e) => {
        let left = e.inputBuffer.getChannelData(0);
        // we clone the samples
        this.leftChannel.push(new Float32Array(left));
        let right = e.inputBuffer.getChannelData(1);
        this.rightChannel.push(new Float32Array(right));
        this.recordingLength += 4096;
      };
      this.state = 'active';
    } catch (err) {
      this.state = 'inactive';
      console.log(err);
    }
  }

  async stop() {
    return new Promise((res, rej) => {
      // to make sure onaudioprocess stops firing
      this.source.disconnect();
      this.audioNode.disconnect();
      this.state = 'inactive';
      let audioBlob = mergeLeftRightBuffers(this.leftChannel, this.rightChannel, this.recordingLength, this.sampleRate);
      res(audioBlob);
    });
  };

  checkState() {
    return this.state;
  }
}

function mergeLeftRightBuffers(lBuffers, rBuffers, rlength, sampleRate) {
  
  function mergeBuffers(channelBuffer, rLength) {
    let result = new Float64Array(rLength);
    let offset = 0;
    let lng = channelBuffer.length;
    for (let i = 0; i < lng; i++) {
      let buffer = channelBuffer[i];
      result.set(buffer, offset);
      offset += buffer.length;
    }
    return result;
  }

  function writeUTFBytes(view, offset, string) {
    let lng = string.length;
    for (let i = 0; i < lng; i++) {
      view.setUint8(offset + i, string.charCodeAt(i));
    }
  }

  function linearInterpolate(before, after, atPoint) {
    return before + (after - before) * atPoint;
  }

  function interleave(leftChannel, rightChannel) {
    var length = leftChannel.length + rightChannel.length;
    var result = new Float64Array(length);
    var inputIndex = 0;
    for (var index = 0; index < length;) {
      result[index++] = leftChannel[inputIndex];
      result[index++] = rightChannel[inputIndex];
      inputIndex++;
    }
    return result;
  }
  
  let leftBuffers = lBuffers.slice(0); //get shallow copy
  let rightBuffers = rBuffers.slice(0);
  let internalInterleavedLength = rlength;

  leftBuffers = mergeBuffers(leftBuffers, internalInterleavedLength);
  rightBuffers = mergeBuffers(rightBuffers, internalInterleavedLength);

  // interleave both channels together
  var interleaved = interleave(leftBuffers, rightBuffers);
  var interleavedLength = interleaved.length;

  // create wav file
  var resultingBufferLength = 44 + interleavedLength * 2;

  var buffer = new ArrayBuffer(resultingBufferLength);

  var view = new DataView(buffer);

  // RIFF chunk descriptor/identifier
  writeUTFBytes(view, 0, 'RIFF');

  // RIFF chunk length
  view.setUint32(4, 44 + interleavedLength * 2, true);

  // RIFF type
  writeUTFBytes(view, 8, 'WAVE');

  // format chunk identifier
  // FMT sub-chunk
  writeUTFBytes(view, 12, 'fmt ');

  // format chunk length
  view.setUint32(16, 16, true);

  // sample format (raw)
  view.setUint16(20, 1, true);

  // stereo (2 channels)
  view.setUint16(22, 2, true);

  // sample rate
  view.setUint32(24, sampleRate, true);

  // byte rate (sample rate * block align)
  view.setUint32(28, sampleRate * 2, true);

  // block align (channel count * bytes per sample)
  view.setUint16(32, 2 * 2, true);

  // bits per sample
  view.setUint16(34, 16, true);

  // data sub-chunk
  // data chunk identifier
  writeUTFBytes(view, 36, 'data');

  // data chunk length
  view.setUint32(40, interleavedLength * 2, true);

  // write the PCM samples
  var lng = interleavedLength;
  var index = 44;
  var volume = 1;
  for (var i = 0; i < lng; i++) {
    view.setInt16(index, interleaved[i] * (0x7FFF * volume), true);
    index += 2;
  }
  // if (cb) {
  //     return cb({
  //         buffer: buffer,
  //         view: view
  //     });
  // }
  let audio =  new Blob([view], {
    type: 'audio/wav'
  });
  console.log(audio);
  return audio;
}







 



