import AudioRecorderMR from "./mediaRecorder.js";
import AudioRecorderAC from "./audioContext.js";

class BrowserType {
  static isFirefox() {
    return typeof InstallTrigger !== "undefined";
  }

  static isSafari() {
    // Safari 3.0+ "[object HTMLElementConstructor]"
    return (
      /constructor/i.test(window.HTMLElement) ||
      (function(p) {
        return p.toString() === "[object SafariRemoteNotification]";
      })(
        !window["safari"] ||
          (typeof safari !== "undefined" && safari.pushNotification)
      )
    );
  }

  static isIE() {
    // Internet Explorer 6-11
    return /*@cc_on!@*/ false || !!document.documentMode;
  }

  static isEdge() {
    // Edge 20+
    return !BrowserType.isIE() && !!window.StyleMedia;
  }

  static isChrome() {
    // Chrome 1 - 71
    return (
      window.navigator.userAgent.toLowerCase().indexOf("chrome") > -1 &&
      !!window.chrome
    );
  }
}

function createAudioElement(blob) {
  let url = window.URL.createObjectURL(blob);
  // let source = document.createElement('source');
  // source.setAttribute('src', url);
  // source.setAttribute('type', 'audio/wav; codecs="1"');
  document.querySelector("audio").src = url;
}

class WebRecorder {
  constructor(ar, btn) {
    this.audioRecorder = ar;
    this.recordBtn = btn;
    this.recordBtn.addEventListener("click", this.recordingActions.bind(this));
    this.saveBtn = document.getElementById("saveBtn");
    this.saveBtn.addEventListener("click", this.saveAudio.bind(this));
    this.audioBlob = null;
  }

  async recordingActions() {
    if (this.audioRecorder.checkState() === "inactive") {
      this.audioRecorder.record();
      this.recordBtn.innerText = "Stop Recording";
      this.saveBtn.style = "display: none";
    } else {
      this.audioBlob = await this.audioRecorder.stop();
      this.recordBtn.innerText = "Start Recording";
      this.saveBtn.style = "display: inline-block";
      createAudioElement(this.audioBlob);
    }
  }

  saveAudio() {
    let a = document.createElement("a");
    a.style = "display: none";
    document.body.appendChild(a);
    a.href = window.URL.createObjectURL(this.audioBlob);
    a.download = "RecordedAudio";
    a.click();
    window.URL.revokeObjectURL(a.href);
  }
}

window.onload = async () => {
  if (!navigator.mediaDevices) {
    console.log("navigator.mediaDevices not available");
    return;
  }

  if (BrowserType.isChrome() || BrowserType.isFirefox()) {
    try {
      let mediaStream = await navigator.mediaDevices.getUserMedia({ audio: true, video: false });
      let audioRecorder = new AudioRecorderMR(mediaStream);
      let webRecorder = new WebRecorder(audioRecorder, document.getElementById('recordBtn'));
    } catch (err) {
      console.log("Error:", err);
    }
  } else if (BrowserType.isIE() || BrowserType.isEdge() || BrowserType.isSafari()) {

    if (!window.AudioContext && !window.webkitAudioContext) {
      console.log("window.AudioContext not available");
      return;
    }

    try {
      let mediaStream = await navigator.mediaDevices.getUserMedia({ audio: true, video: false });
      let audioRecorder = new AudioRecorderAC(mediaStream);
      let webRecorder = new WebRecorder(audioRecorder, document.getElementById('recordBtn'));
    } catch (err) {
      console.log("Error:", err);
    }
  }

  // if (!window.AudioContext && !window.webkitAudioContext) {
  //   console.log("window.AudioContext not available");
  //   return;
  // }

  // try {
  //   let mediaStream = await navigator.mediaDevices.getUserMedia({
  //     audio: true,
  //     video: false
  //   });
  //   let audioRecorder = new AudioRecorderAC(mediaStream);
  //   let webRecorder = new WebRecorder(
  //     audioRecorder,
  //     document.getElementById("recordBtn")
  //   );
  // } catch (err) {
  //   console.log("Error:", err);
  // }
};
